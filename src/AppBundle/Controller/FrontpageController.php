<?php
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Request as Req;

//use Doctrine\DBAL\Driver\PDOException;
//use Symfony\Component\HttpFoundation\Response;



class FrontpageController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method("GET")
     */
    public function indexAction(Request $request)
    {
        $order = new Req();

        $form = $this->createFormBuilder($order)
            ->setAction($this->generateUrl('placeOrder'))
            ->setMethod('POST')
            ->add('customer', 'Symfony\Component\Form\Extension\Core\Type\TextType')
            ->add('feedback', 'Symfony\Component\Form\Extension\Core\Type\TextType')
            ->add('description', 'Symfony\Component\Form\Extension\Core\Type\TextareaType')
            ->add('save', 'Symfony\Component\Form\Extension\Core\Type\SubmitType', array('label' => 'Place order'))
            ->getForm();

        $result = $request->get('result');

        return $this->render('app/index.html.twig', array(
            'form' => $form->createView(),
            'result' => $result
        ));
    }

    /**
     * @Route("/placeOrder", name="placeOrder")
     * @Method("POST")
     */
    public function indexPlaceOrderAction(Request $request)
    {
        //set order Customer, Feedback and Description

        $order = new Req();

        $form = $request->get("form");
        $order->setCustomer( $form["customer"] );
        $order->setFeedback( $form["feedback"] );
        $order->setDescription( $form["description"] );

        //set moderator (by id) as order executor
        $query = $this->getDoctrine()->getManager()
            ->createQuery(
                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_SUPER_ADMIN"%');
        $moderator = $query->getResult();
        //select first moderator from array:
        $moderator = $moderator[0];

        $order->setExecutor($moderator->getId());
        $order->setComment('');
        $order->setStatus('new');

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);

        $em->flush();

        return $this->redirectToRoute('homepage', array(
            'result' => 'success',
            'customer' => $form["customer"],
            'feedback' => $form["feedback"]
        ), 301);
    }


    /**
     * @Route("/moderatorPage", name="moderatorPage")
     * @Method("GET")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function moderatorPageAction(Request $request){

        $moderator = $this->getUser();

        $requestRepository = $this->getDoctrine()
            ->getRepository('AppBundle:Request');

        $query = $this->getDoctrine()->getManager()
            ->createQuery(
                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_ADMIN"%');
        $managers = $query->getResult();

        if( $request->query->get('showCurrent') ){

            //show CURRENT orders (that are in progress)

            $query = $this->getDoctrine()->getManager()
                ->createQuery(
                    "SELECT o FROM AppBundle:Request o WHERE o.executor != ".$moderator->getId()." AND o.status != 'closed' ORDER BY o.id ASC"
                );
            $orders = $query->getResult();

            return $this->render('app/moderatorPage.html.twig', array(
                'orders' => $orders,
                'table' => 'in_progress',
                'managers' => $managers,
            ));

        } elseif( $request->query->get('showClosed') ){

            //show CLOSED orders

            $orders = $requestRepository->findByStatus('closed');

            return $this->render('app/moderatorPage.html.twig', array(
                'orders' => $orders,
                'table' => 'closed',
                'managers' => $managers,
            ));
        } else {

            //show NEW orders (that are in progress)

            $orders = $requestRepository->findBy( array( 'executor' => $moderator->getId(), 'status' => 'new' ) );

            return $this->render('app/moderatorPage.html.twig', array(
                'orders' => $orders,
                'table' => 'new',
                'managers' => $managers,
            ));
        }
    }

    /**
     * @Route("/moderatorSentToManager", name="moderatorSentToManager")
     * @Method("POST")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function moderatorNewSentToManagerAction(Request $request){

        //get POST data:
        $orderId = $request->get('orderId');
        $managerId = $request->get('managerId');

        //Get order by POST data Id
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Request');
        $order = $repository->findOneById($orderId);

        //set new executor
        $order->setExecutor($managerId);
        //$order->setStatus('in_progress');

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        //show NEW ORDERS table by default:
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Request');
        $orders = $repository->findByStatus('new');
        $query = $this->getDoctrine()->getManager()
            ->createQuery(
                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_ADMIN"%');
        $managers = $query->getResult();

        return $this->render('app/moderatorPage.html.twig', array(
            'orders' => $orders,
            'table' => 'new',
            'managers' => $managers,
        ));
    }

    /**
     * @Route("/moderatorClose", name="moderatorClose")
     * @Method("POST")
     * @Security("has_role('ROLE_SUPER_ADMIN')")
     */
    public function moderatorCloseAction(Request $request){

        //get POST data:
        $orderId = $request->get('orderId');

        //Get order by POST data Id
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Request');
        $order = $repository->findOneById($orderId);

        //set new status and comment
        $order->setStatus('closed');
        $order->setComment('Closed by moderator');

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        //show NEW ORDERS table by default:
        $repository = $this->getDoctrine()
            ->getRepository('AppBundle:Request');
        $orders = $repository->findByStatus('new');
        $query = $this->getDoctrine()->getManager()
            ->createQuery(
                'SELECT u FROM AppBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_ADMIN"%');
        $managers = $query->getResult();

        return $this->render('app/moderatorPage.html.twig', array(
            'orders' => $orders,
            'table' => 'new',
            'managers' => $managers,
        ));
    }

    /**
     * @Route("/managerPage", name="managerPage")
     * @Method("GET")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function managerPageAction(Request $request){

        $manager =  $this->getUser();

        $query = $this->getDoctrine()->getManager()
            ->createQuery(
                "SELECT o FROM AppBundle:Request o WHERE o.executor = :manager AND o.status <> 'closed'"
            )->setParameter('manager', $manager);
        $orders = $query->getResult();

        return $this->render('app/managerPage.html.twig', array(
            'orders' => $orders,
            'manager' => $manager
        ));
    }

    /**
     * @Route("/managerChangeStatus", name="managerChangeStatus")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function managerChangeStatusAction(Request $request){

        $manager =  $this->getUser();

        $operation = $request->get('operation');

        $status = $request->get('status');
        $id = $request->get('orderId');

        $order = $this->getDoctrine()
            ->getRepository('AppBundle:Request')
            ->find($id);
        $order->setStatus($status);

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        return new JsonResponse();
    }

    /**
     * @Route("/managerChangeComment", name="managerChangeComment")
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function managerChangeCommentAction(Request $request){

        $manager =  $this->getUser();

        $operation = $request->get('operation');

        $comment = $request->get('comment');
        $id = $request->get('orderId');

        $order = $this->getDoctrine()
            ->getRepository('AppBundle:Request')
            ->find($id);
        $order->setComment($comment);

        $em = $this->getDoctrine()->getManager();
        $em->persist($order);
        $em->flush();

        return new JsonResponse();
    }
}
