<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;


use AppBundle\Entity\Request as Req;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    /**
     *          Route("/", name="homepage")
     */
    /*public function indexAction(Request $request)
    {
        // replace this example code with whatever you need
        return $this->render('default/index.html.twig', array(
            'base_dir' => realpath($this->container->getParameter('kernel.root_dir').'/..'),
        ));
    }*/

    /**
     * @Route("/create", name="create")
     */
    public function createAction()
    {
        $request = new Req();
        $request->setExecutor('me');
        $request->setStatus('Open');
        $request->setComment('ololo');
        $request->setDiscription('desc');

        $em = $this->getDoctrine()->getManager();

        $em->persist($request);
        $em->flush();

        return new Response('Created order id '.$request->getId());
    }

    /**
     * @Route("/show/{id}", name="show")
     */
    public function showAction($id)
    {
        $request = $this->getDoctrine()
            ->getRepository('AppBundle:Request')
            ->find($id);

        if (!$request) {
            throw $this->createNotFoundException(
                'No request found for id '.$id
            );
        }

        return new Response('Request executor is: '.$request->getExecutor());
    }
}
